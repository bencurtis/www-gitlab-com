---
layout: markdown_page
title: "LaunchDarkly"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
Launchdarkly is a feature delivery and feature management service offering.  This means that the service they provide allows their customers to dynamically control when new features (within their application) become available to their end users.  Launchdarkly’s service offering represents a tool within the DevOps Toolchain that aligns within the Continuous Delivery (CD) tool category.

## Strengths
Launchdarkly provides a very sophisticated feature management service that enables Development and Operations teams to deliver code faster with minimal risk by using feature flags to deploy code when they want while keeping new features hidden until product and marketing teams are ready to share.   Their service allows customers to take full control of feature releases through:
 
   -Fast feature delivery: separating code deployments from feature deliveries
   -Incremental changes: controlling feature rollouts into predefined stages and slowly test the functionality within            production before confirming it’s safe to proceed to the next step
   -Canary launches: rolling features out to a small number of users to assess the reaction of the overall system (also          known as Percentage Releases)
   -Dynamic feature control: coordinating and delegating release control to other teams in your organization, turning off        features when they are causing performance issues or poor user experiences (feature kill switch), and retiring features.
   -Automated feature delivery: setup scripts that automatically turn features off/on or adjust a flag’s targeting rules         based on metrics from Application Performance Management (APM) tools
   -Analytics: providing deep insights across all features and how they are performing with Flag Insights; understanding how     the application and team members are using LaunchDarkly.

## Gaps
Launchdarkly is a niche solution that must be overlaid within a customer's existing DevOps Toolchain.  Though they do provide more comprehensive feature flag configuration capabilities, they lack:

   -A Holistic DevOps Solution: unable to meet customers complete DevOps needs with a single application Toolchain
   -[Reasonable/Economical pricing](https://about.gitlab.com/pricing/): high per-user price point for a single tool              (Launchdarkly’s pricing starts at $90/user for their lower tier plan, $390/user for their mid-tier plan and they don't       offer a free base tier)
   -[Industry recognition](https://about.gitlab.com/analysts/): no industry recognition by research and advisory bodies such     as Gartner and Forrester.
   -[Meeting/Phone Support](https://about.gitlab.com/support/#phone-support): no option to request escalating a technical        support inquiry to a troubleshooting call/meeting

Gitlab’s DevOps Toolchain includes [feature flags for feature management] (https://about.gitlab.com/direction/release/feature_flags/) as Launchdarkly does.  Unlike Launchdarkly, Gitlab’s Toolchain offers deeper features/functionalities necessary for quickly delivering high quality applications and services for the entire software lifecycle in a single application.  Examples of GitLab features/functionalities that are not offered by Launchdarkly include  [Source Code Management (SCM)](https://about.gitlab.com/stages-devops-lifecycle/source-code-management/), [Continuous Integration (CI)](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/) and [Security Embedded within the DevOps workflow (DevSecOps)](https://about.gitlab.com/solutions/dev-sec-ops/).

## Resources
[Launchdarkly.com]
(https://launchdarkly.com/)

[Launchdarkly Pricing]
(https://launchdarkly.com/pricing/)

[Launchdarkly Wikipedia]
(https://en.wikipedia.org/wiki/Feature_toggle)



## Comparison
