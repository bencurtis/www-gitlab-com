--- 
layout: handbook-page-toc
title: "Investor Relations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Earnings Release Calendar (day of quarter)
Day 7: Investor Relations (IR) Debrief

*  Attendees: CEO, CFO, CLO, and IR
*  Discussion topic: Identify key developments from the prior quarter for inclusion in the shareholder letter

Day 10: Begin external financial audit - DRI is PAO

Day 12: Preliminary financial results and key metrics

*  Attendees: CFO, PAO, VP FP&A, and IR
*  Discussion topic: Drivers of financial and operating metric overperformance and underperformance to expectations

Day 20: Begin legal review of documents - DRI is IR

Day 25: Submit 10-Q and press release to Audit Committee - DRI is PAO

Day 30: [Disclosure Committee](/handbook/internal-audit/sarbanes-oxley/#disclosure-committee-charter) meeting

*  Attendees: CLO, PAO, VP of FP&A, and IR
*  Discussion topics: Approve shareholder letter, earnings release including final financial tables, investor presentation, and quarterly or annual SEC filing

Day 35: Release financial results including requisite SEC filings and host earnings call

*  Attendees: CEO, CFO, and IR
*  Discussion topic: Moderated question and answer session regarding past performance and outlook as of the reporting date  

Continuous updates: Abridged version of financials and key operating metrics, consensus, and Q&A tracker 

## Trading Window

We anticipate that our quarterly trading window will open the third trading day after the announcement of our quarterly results and that it will close again immediately prior to the last four weeks of the fiscal quarter, as indicated by an announcement made by the CLO. However, it is important to note that any quarterly trading window may be cut short if the CLO determines that material nonpublic information exists in such a fashion that it would make trades by directors, employees, and consultants inappropriate.

## Performance Indicator

### Enterprise Value to Sales
Enterprise Value to Sales compares the enterprise value (EV) of a company to its annual sales.

Enterprise Value to Sales = Enterprise Value/Annual Sales

Enterprise Value = Market Capitalization + Debt - Cash and Cash Equivalents
