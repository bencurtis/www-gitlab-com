---
layout: markdown_page
title: "SLC.1.02 - Source Code Management"
---
 
## On this page
{:.no_toc}
 
- TOC
{:toc}
 
# SLC.1.02 - Source Code Management
 
## Control Statement
Source code is managed with GitLab-approved version control mechanisms.
 
## Context

GitLab-approved version control mechanisms come with strong security and SDLC maturity.
 
## Scope
This control applies to:
   * GitLab.com
   * System that support the operation of GitLab.com
   * All applications that store or process financial data

## Ownership
TBD
 
## Guidance
TBD
 
## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Source Code Management issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/889) . 
 
### Policy Reference
TBD
 
## Framework Mapping
TBD