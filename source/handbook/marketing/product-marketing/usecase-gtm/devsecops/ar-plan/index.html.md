---
layout: markdown_page
title: "DevSecOps AR Plan"
---


## Overall DevSecOps plan for the CI use case

| Firm | Gartner | Forrester | IDC | Other |
|--------------------|------------------------------|-----------|-----|-------|
| Key Analysts | <list key analysts> |  |  |  |
| Recent research | <links to relevant research> |  |  |  |
| Briefing frequency | <monthly, quarterly, etc> |  |  |  |
| Briefing Agenda Doc | <link to the briefing G-Doc> |  |  |  |
