---
layout: markdown_page
title: GitLab Scaling and High Availability
---
GitLab is a modular, highly scalable application [distributed in a variety of packaged formats](https://docs.gitlab.com/ee/install/). Properly architected it can provide for companies with fewer than 10 employees to companies with hundreds of thousands. 

This page discusses some of the high-level considerations necessary when you’re deciding which solution is right for your business needs and budget restrictions. 

For technical details, please see our [Scaling and High Availability page](https://docs.gitlab.com/ee/administration/high_availability/).

GitLab provides a service that is usually essential to most organizations. Therefore, any downtime should be short and planned. Through our Omnibus distribution GitLab provides a reliable service even on a single server without special configuration. As your organization grows, GitLab has the flexibility to expand according to your business needs.

For example, 
 - Growth in headcount might mean you’ll need to allocate additional resources to your existing installation.
 - You may have strict uptime requirements which call for High Availability (HA) solutions and/or zero-downtime upgrades.
 - Expanding into new geographical regions means that your teams need performant access to GitLab.
 - Your business may require plans for quick restoration of service in the case of a disaster.

Properly configured, GitLab can address all of the above cases.

**Keep in mind that all solutions come with a trade-off between cost/complexity and uptime.** The more uptime you want, the more complex the solution. And the more complex the solution, the more work is involved in setting up and maintaining it. Complex requirements require an investment in time and resources, and any architecture should balance the costs against the benefits.

----

### What's on this page
{:.no_toc}

- TOC
{:toc}

---


## What are my needs?

The two questions that you need to ask yourself are:

 1. What are my specific needs for GitLab availability?
 1. What complexity level can my organization handle?

GitLab architectures can range from a simple single-node installation to a highly complex and costly primary-primary highly available configurations. When it comes to High Availability, there is no way to split between an increase in complexity and an increase in cost.

We recommend that you thoroughly analyze the benefits of an HA solution against its costs. It is important that you always choose something that you have experience with and will test regularly. 

> **A badly implemented HA solution causes more downtime than it solves.**

It is more cost effective to go from a simple solution to a more complex one than the other way around. Therefore, we suggest you work your way up from the simplest solution and see what works best for you.

## Maturity levels

### Level 1: Single-node Omnibus installation

This solution is appropriate for many teams that have a single server at their disposal. With automatic backup of the GitLab repositories, configuration, and the database, this can be an optimal solution if you don't have strict availability requirements. 

This configuration is supported in [GitLab Starter, Premium and Ultimate](https://about.gitlab.com/pricing/).

References:
 - [Installation Docs](https://docs.gitlab.com/ee/install/)
 - [Backup/Restore Docs](https://docs.gitlab.com/omnibus/settings/backups.html#backup-and-restore-omnibus-gitlab-configuration)

### Level 2: Multiple application servers

By separating components you can see a number of advantages compared to a single-node setup. Namely, you can:

 - Increase the number of users 
 - Enable zero-downtime upgrades 
 - Increase availability 

Additional application nodes will handle frontend traffic, with a load balancer in front to distribute traffic across those nodes. Meanwhile, each application node connects to a shared file server and DB systems on the back end. This way, if one of the application servers fails, the workflow is not interrupted.

This configuration is supported in [GitLab Starter, Premium and Ultimate](https://about.gitlab.com/pricing/).

References:
 - no references available

### Level 3: Highly Available

By adding automatic failover for DB systems, we can enable higher uptime with an additional layer of complexity.

This configuration is supported in [GitLab Premium and Ultimate](https://about.gitlab.com/pricing/).

References: 

 - [High Availability Architecture Examples](https://docs.gitlab.com/12.5/ee/administration/high_availability/README.html#high-availability-architecture-examples)

### Level 4: GitLab Geo

GitLab Geo allows you to replicate your GitLab instance to other geographical locations as a read-only fully operational instance that can also be promoted in case of disaster. 

This configuration is supported in [GitLab Premium and Ultimate](https://about.gitlab.com/pricing/).

References:

 - [Geo Documentation](http://docs.gitlab.com/ee/gitlab-geo/README.html)
 - [GitLab Geo with a highly available configuration](https://docs.gitlab.com/ee/administration/geo/replication/high_availability.html)

# Please get in touch
Helping you choose the configuration that is right for your business is something we take very seriously at GitLab. The information we provide here is a rough guide designed to help you get a feel for the required resources and expertise required to scale GitLab appropriately. When it comes to your specific situation, we prefer to assess it in its context. We strongly suggest you get in touch with us before setting it up. Support for HA and Geo are included with [GitLab Premium and Ultimate](https://about.gitlab.com/pricing/). If you have any questions please [contact us](https://about.gitlab.com/sales/).
